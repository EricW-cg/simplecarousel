using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.EventSystems;

using MEC; 

// In ReadyController project
namespace SimpleCarousel
{
  /// <summary>
  /// TrueX, RectTrans, AnImage, ACanvas
  /// </summary>
  // struct vs class, jobs and burst require structs?
  // array of structs can save to l1, l2, l3 cpu cache
  // list of classes can not? also struct must not contain
  // classes, structs should not be mutable? Use readonly?




  public delegate void ItemIndexUpdatedDelegate(int itemIndex);

  // this can be a singleton if it can run all carousels!
  // that means the container and list has to be passed in each time.
  // the coroutines can run and stopped individually under their own tag
  public class Animator
    // write my own
    //IPointerDownHandler, IPointerUpHandler,
    //IBeginDragHandler, IDragHandler, IEndDragHandler,
    //IScrollHandler
  {

    public ItemIndexUpdatedDelegate ItemIndexUpdatedEvent; // who's subscribed to this?
    //public Action<int> ItemIndexUpdated; // use this instead of delegate?

    // animation
    // inaccessable to to protection level, temp public
    //public Coroutine _drift;
    // inaccessable to to protection level, temp public
    public bool _isDrifting = false;

    // for CarouselStats.cs
    [HideInInspector] public float DriftAmount = 0;



    public Animator() { }

    public void DoAwake()
    {
      // hiding temporarily, check ButtonEvents.cs
      // swap coupled directions, put animator in buttons
      //_carousel.Previous.onClick.AddListener(() => OnClickPrevious());
      //_carousel.Next.onClick.AddListener(() => OnClickNext());


      
    }


    #region Mouse wheel scroll handlers

    public void OnScroll(PointerEventData e, Carousel carousel)
    {
      // StopMoveCoroutines(); // used in CarouselStats.cs?

      if (e.scrollDelta.y < 0f) { ScrollRight(e, carousel); }
      else if (e.scrollDelta.y > 0f) { ScrollLeft(e, carousel); }

    }

    // scroll up on mouse wheel, change to scroll forward?
    private void ScrollLeft(PointerEventData e, Carousel carousel)
    {
      DriftAmount--; // e.scrollDelta.y is 6 or -6 right now
      carousel.CurrentIndex = (carousel.CurrentIndex + 1) % carousel.ItemsList.Count;
      ItemIndexUpdatedEvent?.Invoke(carousel.CurrentIndex);

      if (_isDrifting) { return; }
      SetDirection(-1, carousel);
      //_drift = _carousel.StartCoroutine(Drift()); // need a monobehaviour! temp commented
      Timing.RunCoroutine(Drift(carousel));
    }

    // scroll down on mouse wheel, change to scroll reverse?
    private void ScrollRight(PointerEventData e, Carousel carousel)
    {
      DriftAmount++; // e.scrollDelta.y is 6 or -6 right now
      carousel.CurrentIndex = Utilities.Mod(carousel.CurrentIndex - 1, carousel.ItemsList.Count);
      ItemIndexUpdatedEvent?.Invoke(carousel.CurrentIndex);

      if (_isDrifting) { return; }
      SetDirection(1, carousel);
      //_drift = _carousel.StartCoroutine(Drift()); // need a monobehaviour temp commented
    }

    #endregion Mouse wheel scroll handlers


    #region Pointer handlers

    public void OnPointerDown(PointerEventData e, Carousel carousel)
    {
      // swap to MEC, MEC in Carousel? Use GameObject container.name
      // so hopefully other carousels are named different, weapons, characters, etc.
      //StopMoveCoroutines(); // move to Utilities
      
    }

    public void OnPointerUp(PointerEventData e, Carousel carousel)
    {
      if (e.dragging) { return; }

      // still needed to wait till end of frame
      //_carousel.StartCoroutine(MoveToItem(e.hovered)); // temp comment needs monobehaviour
    }



    #endregion Pointer handlers


    #region Drag handlers

    public void OnBeginDrag(PointerEventData e, Carousel carousel) { } // not used at the moment

    public void OnDrag(PointerEventData e, Initializer initializer, Carousel carousel)
    {
      SetDirection(e.delta.x, carousel);

      // dragSpeed in the inspector is set to .35, sometimes dragging works
      // sometimes it drags really really slow.
      UpdatePositions(e.delta.x * initializer.DragSpeed * Time.deltaTime, carousel);
    }

    public void OnEndDrag(PointerEventData e,Initializer initializer, ItemsModifier itemsModifier, Carousel carousel)
    {
      // clamped to prevent super fast
      DriftAmount = (int)(Mathf.Clamp(e.delta.x, -300, 300) * initializer.DragSpeed * Time.deltaTime);

      int nextIndex = itemsModifier.GetNextIndex(carousel);
      carousel.CurrentIndex = Utilities.Mod(nextIndex + (int)(DriftAmount * -1), carousel.ItemsList.Count);
      ItemIndexUpdatedEvent?.Invoke(carousel.CurrentIndex);

      // self correct for drag offset so drift doesn't end between items
      DriftAmount += (itemsModifier.DistanceToIndex(nextIndex, carousel) / carousel.DistanceBetweenItems) * -1;


      //if (_direction > 0)

      // these all need to run from their own carousel.
      //_drift = _carousel.StartCoroutine(Drift());// temp hiddin, needs monobehaviour

      // move this out and into Items.cs?
      //int GetNextIndex()


    }

    #endregion Drag handlers


    #region Position, direction, index, animation


    //private IEnumerator OnPointerUpCoroutine(PointerEventData e)
    public IEnumerator MoveToItem(List<GameObject> hovered, Carousel carousel, ItemsModifier itemsModifier)
    {
      // still needed to wait till end of frame?
      yield return new WaitForEndOfFrame();

      foreach (GameObject item in hovered)
      {
        if (!item.transform.parent) { continue; }
        else if (item.transform.parent.gameObject == carousel.Container)
        {
          carousel.CurrentIndex = item.transform.GetSiblingIndex();
          ItemIndexUpdatedEvent?.Invoke(carousel.CurrentIndex);

          break;
        }
      }

      float delta = carousel.ItemsList[carousel.CurrentIndex].TrueX - carousel.DistanceBetweenItems;
      SetDirection(delta, carousel); // rectTransform?
      DriftAmount = (itemsModifier.DistanceToIndex(carousel.CurrentIndex, carousel) / carousel.DistanceBetweenItems) * -1;
      //_drift = _carousel.StartCoroutine(Drift()); // temp commented items needs coroutine
    }

    // inaccessable to to protection level, temp public
    //public IEnumerator Drift(Carousel carousel)
    public IEnumerator<float> Drift(Carousel carousel)
    {
      _isDrifting = true;
      float offset;
      float speed = 2f; // maybe move this up and serializedfield or public

      // if right or left clicked before drift stops at just the
      // right time it could cause it to pass over the end and
      // go infinitely, I think
      while (Mathf.Abs(DriftAmount) > 0.005f)
      {
        offset = DriftAmount * carousel.DistanceBetweenItems * speed * Time.deltaTime;
        DriftAmount = DriftAmount - offset / carousel.DistanceBetweenItems;

        UpdatePositions(offset, carousel);

        //yield return null; // Unity coroutine
        yield return Timing.WaitForOneFrame;
      }

      _isDrifting = false;
      //_carousel.Items.ItemsList.Count; will eventually be wrong
      carousel.DistanceBetweenItems = carousel.ContainerWidth / carousel.ItemsList.Count;


      // still useful? Maybe? Move to Items.cs? Commented out for now.
      //_currentIndex = GetClosestIndex();
      int GetClosestIndex()
      {
        foreach (Item item in carousel.ItemsList)
        {
          if (Mathf.Abs(item.TrueX - carousel.ContainerCenter) < (carousel.DistanceBetweenItems / 2))
          {
            return carousel.ItemsList.IndexOf(item);
          }
        }

        return -1; // fail, maybe a try catch above instead?
      } // used to set current index after drift stops
    }

    // inaccessable to to protection level, temp public
    public void SetDirection(float delta, Carousel carousel)
    {
      // direction is also set with ScrollLeft and ScrollRight
      if (delta > 0) { carousel.Direction = 1; }
      else if (delta < 0) { carousel.Direction = -1; }
    }

    // move to CarouselItems.cs or modify to work with
    // DistanceToIndex moved

    // to disconnect this from Items
    public void UpdatePositions(float offset, Carousel carousel)
    {
      #region notes
      // ideas

      // use two lists, one of hidden items and one of visible
      // and swap between them using list methods
      // public void Insert(int index, T item);
      // public bool Remove(T item);
      // public void RemoveAt(int index);

      // pooling gameobjects unity way
      // instead of going through _carouselItems maybe transform.children or whatever
      // and reparent items into another standby gameobject while they aren't in view
      // or https://docs.unity3d.com/2021.1/Documentation/ScriptReference/Pool.ObjectPool_1.html

      // pooling objects c# way using concurrentbag<T>, hmm
      // https://docs.microsoft.com/en-us/dotnet/standard/collections/thread-safe/how-to-create-an-object-pool

      #endregion

      // float basePosX = _carouselItemsTrueX[0] + offset;
      float basePosX = carousel.ItemsList[0].TrueX + offset; // null reference.
      float lerpValue;

      float x;

      float maxScale = 2f;
      float scale;
      byte alpha;
      int sortOrder;

      // should swap back to a for statement to animate a max of 5
      //for (int i = 0; i < _items.Count; i++)
      foreach (Item item in carousel.ItemsList)
      {
        // this will allow CarouselItem to be a struct instead of class
        // and item.TrueX won't throw an error but is very slow, I think
        // then doing modifications and _items[i] = item at the end
        //CarouselItem item = _items[i];

        x = basePosX + (carousel.DistanceBetweenItems * carousel.ItemsList.IndexOf(item));

        // wrap
        if (x > carousel.ContainerMax) { x = (x % carousel.ContainerMax) + carousel.ContainerMin; }
        else if (x < carousel.ContainerMin) { x = carousel.ContainerMax - (carousel.ContainerMin - x); }

        // set item TrueX before adjusting with curves
        //item.TrueX = x; // _items[i].TrueX = x;
        carousel.ItemsList[0].TrueX = x; // _items[i].TrueX = x;
                                                //if (item.RectTrans.name == "shadowAvatar") { _shadowXTmp.text = x.ToString(); } // temp

        // lerpValue = Mathf.Lerp(0, 1, item.TrueX / _containerWidth);
        lerpValue = Mathf.Lerp(0, 1, (item.TrueX - carousel.ContainerMin) / carousel.DistanceBetweenItems);

        // calculate item values
        x = (carousel.CurvePosition.Evaluate(lerpValue) * carousel.DistanceBetweenItems) + carousel.ContainerMin;
        scale = carousel.CurveScale.Evaluate(lerpValue) * maxScale;
        // byte r, g, b; r = g = b = 255; // clever? 
        alpha = (byte)(carousel.CurveAlpha.Evaluate(lerpValue) * 255f);
        sortOrder = (int)(scale * 100); // swap 100 for items.count + 1 so sortorder is small as possible?

        // set item values
        item.RectTrans.position = new Vector2(x, item.RectTrans.position.y);
        item.RectTrans.localScale = new Vector2(scale, scale);
        // item.AnImage.color = new Color32(r, g, b, alpha);
        // item.AnImage.color = new Color32(255, 255, 255, alpha);
        item.ACanvas.sortingOrder = sortOrder;

        // to allow custom tint adjust
        item.AnImage.color = new Color32(
            (byte)(item.AnImage.color.r * 255f),
            (byte)(item.AnImage.color.g * 255f),
            (byte)(item.AnImage.color.b * 255f),
            alpha);

        // can do struct with item.TrueX but I think makes it slow
        //_items[i] = item;
      }
    }

    #endregion Position, direction, index, animation


    #region Helpers

    // move to Utilities
    public void StopMoveCoroutines(Carousel carousel)
    {
      _isDrifting = false;

      

      if (carousel.Drift != null)
      {
        //_carousel.StopCoroutine(_drift);// temp commented stop coroutine tag from Items

        //this should be the update _currentIndex spot
      }
    }



    #endregion Helpers
  }
}