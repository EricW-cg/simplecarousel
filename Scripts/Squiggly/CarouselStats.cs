using System.Collections;
using System.Collections.Generic;

using TMPro;
using UnityEngine.UI;
using UnityEngine;
using UnityEngine.EventSystems;

using SimpleCarousel;

namespace SimpleCarouselEditor
{
  // this goes on Items gameobject next to CarouselAnimator.cs
  // can't put this in editor folder due to monobehavior
  public class CarouselStats : MonoBehaviour,
      //IPointerDownHandler, IPointerUpHandler,
      IBeginDragHandler, IDragHandler, IEndDragHandler,
      IScrollHandler
  {
    [SerializeField] private GameObject _info; // use this after move

    // buttons
    private Button _scrollLeft;
    private Button _scrollRight;

    private TextMeshProUGUI _clickDeltaTmp; // clickDelta (TMP)
    private TextMeshProUGUI _scrollDeltaTmp;
    private TextMeshProUGUI _dragDeltaTmp;
    private TextMeshProUGUI _endDragDeltaTmp;

    // are these used? Check CarouselAnimator.cs
    private TextMeshProUGUI _offsetTmp;
    private TextMeshProUGUI _timedeltaTimeTmp;

    private TextMeshProUGUI _containerWidthTmp;
    private TextMeshProUGUI _containerXminTmp;
    private TextMeshProUGUI _containerXcenterTmp;
    private TextMeshProUGUI _containerXmaxTmp;

    // these two require a coroutine for a few seconds at least to
    // run smoothly, at the moment they are updated from interface events
    // indexDriftCount is the index count until the selected character
    private TextMeshProUGUI _shadowXTmp;
    private TextMeshProUGUI _indexDriftCountTmp;


    // This script doesn't use coroutines so Shadow
    // can only be updated from the interface events
    // not as nice but it'll work fine.
    private Item _shadow; // items[0]
    private SimpleCarousel.Animator _animator;


    public void Awake()
    {
      // prevents button listeners from starting but not container size, so weird
      if (!enabled) { Debug.Log("disabled"); return; }

      if (_info == null)
      {
        Debug.LogError("Add Info gameobject from Carousel stats to the info field", this.gameObject);
      }

      _animator = GetComponent<SimpleCarousel.Animator>();

      foreach (Button child in transform.parent.GetComponentsInChildren<Button>())
      {
        if (child.gameObject.name == "ScrollLeft") { _scrollLeft = child; }
        if (child.gameObject.name == "ScrollRight") { _scrollRight = child; }
      }
      _scrollLeft.onClick.AddListener(() => OnClickLeft());
      _scrollRight.onClick.AddListener(() => OnClickRight());


      foreach (TextMeshProUGUI child in _info.transform.GetComponentsInChildren<TextMeshProUGUI>())
      {
        if (child.gameObject.name == "clickDelta (TMP)") { _clickDeltaTmp = child; }
        if (child.gameObject.name == "scrollDelta (TMP)") { _scrollDeltaTmp = child; }
        if (child.gameObject.name == "dragDelta (TMP)") { _dragDeltaTmp = child; }
        if (child.gameObject.name == "endDragDelta (TMP)") { _endDragDeltaTmp = child; }

        // don't think these are used in CarouselAnimator.cs at the moment
        if (child.gameObject.name == "driftSpeedOffset (TMP)") { _offsetTmp = child; }
        if (child.gameObject.name == "Time.deltaTime (TMP)") { _timedeltaTimeTmp = child; }


        if (child.gameObject.name == "containerWidth (TMP)") { _containerWidthTmp = child; }
        if (child.gameObject.name == "containerXmin (TMP)") { _containerXminTmp = child; }
        if (child.gameObject.name == "containerXcenter (TMP)") { _containerXcenterTmp = child; }
        if (child.gameObject.name == "containerXmax (TMP)") { _containerXmaxTmp = child; }


        if (child.gameObject.name == "shadowX (TMP)") { _shadowXTmp = child; }
        if (child.gameObject.name == "indexDriftCount (TMP)") { _indexDriftCountTmp = child; }
      }


      // to get width and min/max
      RectTransform rt = GetComponent<RectTransform>();

      // carousel item container width
      float _containerWidth = rt.rect.width;

      // set min and max x values
      // changing resolutions give very different results
      Vector3[] v = new Vector3[4];
      rt.GetWorldCorners(v);
      float _xMin = v[0].x;
      float _xMax = v[3].x;
      float _xCenter = ((_xMax - _xMin) / 2) + _xMin;

      // these seem to run even what CarouselStats.cs is disabled???
      _containerWidthTmp.text = _containerWidth.ToString();
      _containerXminTmp.text = _xMin.ToString();
      _containerXcenterTmp.text = _xCenter.ToString();
      _containerXmaxTmp.text = _xMax.ToString();

    }

    private void Start()
    {

      //_shadow = GetComponent<SimpleCarousel.Animator>().items[0]; 
      //_shadow = _carousel.ItemsContainerData._items[0];
    }

    private void OnClickLeft()
    {
      _clickDeltaTmp.text = "-1";
      _shadowXTmp.text = _shadow.TrueX.ToString();
      _indexDriftCountTmp.text = _animator.DriftAmount.ToString();
    }
    private void OnClickRight()
    {
      _clickDeltaTmp.text = "1";
      _shadowXTmp.text = _shadow.TrueX.ToString();
      _indexDriftCountTmp.text = _animator.DriftAmount.ToString();
    }


    public void OnScroll(PointerEventData e)
    {
      _scrollDeltaTmp.text = (e.scrollDelta.y * -1).ToString();
      _shadowXTmp.text = _shadow.TrueX.ToString();
      _indexDriftCountTmp.text = _animator.DriftAmount.ToString();
    }

    public void OnBeginDrag(PointerEventData e) { } // check CarouselAnimator.cs
    public void OnDrag(PointerEventData e)
    {
      _dragDeltaTmp.text = e.delta.ToString();
      _shadowXTmp.text = _shadow.TrueX.ToString();
      _indexDriftCountTmp.text = _animator.DriftAmount.ToString();
    }
    public void OnEndDrag(PointerEventData e)
    {
      _endDragDeltaTmp.text = e.delta.ToString(); // temp
      _indexDriftCountTmp.text = _animator.DriftAmount.ToString();
    }

  }
}
