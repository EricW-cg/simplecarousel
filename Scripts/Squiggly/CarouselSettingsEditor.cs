using UnityEngine;
using UnityEditor;
using SimpleCarousel;

namespace SimpleCarouselEditor
{

  [CustomEditor(typeof(Initializer))]
  public class CarouselSettingsEditor : Editor
  {
    // whole object? What about the class specifically?
    private SerializedObject _carouselSettings;

    // Custom Curves
    private SerializedProperty _curvePosition;
    private SerializedProperty _curveScale;
    private SerializedProperty _curveAlpha;

    private void OnEnable()
    {
      _carouselSettings = new SerializedObject(target);

      _curvePosition = _carouselSettings.FindProperty("_curvePosition");
      _curveScale = _carouselSettings.FindProperty("_curveScale");
      _curveAlpha = _carouselSettings.FindProperty("_curveAlpha");
    }

    public override void OnInspectorGUI()
    {
      // this puts the stupid script name title field above it
      // so moved curves to bottom and button last
      base.OnInspectorGUI(); // why use this?

      _carouselSettings.Update();

      //EditorGUILayout.LabelField("Animation Curves", EditorStyles.boldLabel);
      if (GUILayout.Button("Reset Curves"))
      {
        _curvePosition.animationCurveValue = ResetCurvePosition();
        _curveScale.animationCurveValue = ResetCurveScale();
        _curveAlpha.animationCurveValue = ResetCurveAlpha();
      }

      // don't need?
      //EditorGUILayout.PropertyField(_curvePosition);
      //EditorGUILayout.PropertyField(_curveScale);
      //EditorGUILayout.PropertyField(_curveAlpha);

      _carouselSettings.ApplyModifiedProperties();
    }

    // These might fail in a build? Or won't matter?
    public static AnimationCurve ResetCurvePosition()
    {
      Keyframe posKey0 = new Keyframe(0f, 0f);
      Keyframe posKey1 = new Keyframe(.5f, .5f);
      posKey1.inTangent = 2f; posKey1.outTangent = 2f;
      Keyframe posKey2 = new Keyframe(1f, 1f);

      return new AnimationCurve(posKey0, posKey1, posKey2);
    }
    public static AnimationCurve ResetCurveScale()
    {
      Keyframe scaleKey0 = new Keyframe(0f, .5f);
      scaleKey0.inTangent = 2.44f; scaleKey0.outTangent = 2.44f;
      Keyframe scaleKey1 = new Keyframe(.5f, 1f);
      Keyframe scaleKey2 = new Keyframe(1f, .5f);
      scaleKey2.inTangent = -2.69f; scaleKey2.outTangent = -2.69f;

      return new AnimationCurve(scaleKey0, scaleKey1, scaleKey2);
    }
    public static AnimationCurve ResetCurveAlpha()
    {
      Keyframe curveKey0 = new Keyframe(0f, 0f);
      curveKey0.inTangent = 4.03f; curveKey0.outTangent = 4.03f;
      Keyframe curveKey1 = new Keyframe(.25f, 1f);
      Keyframe curveKey2 = new Keyframe(.75f, 1f);
      Keyframe curveKey3 = new Keyframe(1f, 0f);
      curveKey3.inTangent = -4.2f; curveKey3.outTangent = -4.2f;

      return new AnimationCurve(curveKey0, curveKey1, curveKey2, curveKey3);
    }

  }
}