
namespace SimpleCarousel
{
  // practicing interfaces.
  // Generic Interface vs generic method
  // method has to stay generic in implementation so no default
  // Interface defines type on implementation (yes and) so specific
  // types can have default values string blah = "blah"
  public interface IAwakable<T>
  {
    // method where constraint breaks the interface method
    // void DoAwake<T>(T t = default);// where T : class { }

    void DoAwake(T t);
  }
}