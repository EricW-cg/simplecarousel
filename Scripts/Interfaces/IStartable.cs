
namespace SimpleCarousel
{
  // practicing interfaces.
  // Generic Interface vs generic method
  // method has to stay generic in implementation so no default
  // Interface defines type on implementation (yes and) so specific
  // types can have default values string blah = "blah"
  public interface IStartable<T>
  {
    // method where constraint just breaks the interface method
    // public void DoStart<T>(T t = default); // where T : class { }
    // public T DoStart {get; set;} // this works but can't pass empty type

    void DoStart(T t);
  }
}