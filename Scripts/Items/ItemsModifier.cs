using System.Collections;
using System.Collections.Generic;


using System.Linq; // .Last() .First() .ElementAt(index)

using UnityEngine;
using UnityEngine.UI;

namespace SimpleCarousel
{
  public class ItemsModifier : IAwakable<Initializer>
  {




    // search for this
    // move to CarouselItems.cs

    // this script should be responsible for:
    // the actual item list
    // current index
    // adding and removing of gameobjects

    //private Manager _carousel;




    // some default items, avatar related
    public const string AddAvatar = "AddAvatar";
    public const string SquidFemale = "b88fab23-f36b-44d2-ba79-45569d47a91e";
    public const string SquidMale = "b27e43d2-0a3c-4660-9ccc-a0203127f097";

    // from CarouselAnimator.cs, was private set, temp public set
    //public int CurrentIndex { get; set; } = 0;

    // could this data be a struct? I break structs so a class? Maybe called Dimensions?
    // this could be the first extendable class?? xMin and xMax use empty objects instead.
    // inaccessable to to protection level, temp public
    //public float _containerWidth;       // swap this out with bounds of carousel eventually
    //public float _xMin;                // panel width / 2 when ready
    //public float _xMax;                // panel width / 2 when ready
    //public float _xCenter;              // panel width / 2 when ready
    //// inaccessable to to protection level, temp public
    //public float _distanceBetweenItems; // panel width / carouselItems.Count


    // needed?
    //private CarouselManager _carousel; 
    //public CarouselItems(CarouselManager carousel) 
    //{
    //  _carousel = carousel;
    //}

    // Add, Menu should deal with button?
    // Remove , Menu should deal with button

    // Append after index, Remove Index
    // GetCurrentIndex? GetItemAtIndex?
    // SetCurrentIndex?

    // Skipping constructor using DoAwake and DoStart
    public ItemsModifier() { }

    // are all these needed? If this class is static
    // or singleton then none of these can be here
    private Initializer _initializer;
    private GameObject _itemsContainer;
    private ButtonEvents _buttonEvents;
    private ButtonStates _buttonStates;
    private Carousel _carousel;
    private Animator _animator;

    // remove this to make static or singleton
    public void DoAwake(Initializer initializer)
    {
      _initializer = initializer;
      _itemsContainer = initializer.ItemsContainer;
      _buttonEvents = initializer.ButtonEvents;
      _buttonStates = initializer.ButtonStates;
      _carousel = initializer.carousel;
      _animator = initializer.Animator;



    }

    //public void DoStart()
    //{
    //  BuildItemList();
    //}





    public float DistanceToIndex(int index, Carousel carousel) => 
      carousel.ItemsList[index].TrueX - carousel.ContainerCenter;

    // move to Items? No?
    public int GetNextIndex(Carousel carousel)
    {
      // direction could be a parameter? 
      // But direction could be too useful in rest of script. So..No?

      foreach (Item item in _initializer.carousel.ItemsList)
      {
        // if the curves are adjusted weird then maybe use TrueX???
        // item.RectTrans.position.x vs item.TrueX
        if (Mathf.Abs(item.TrueX - _carousel.ContainerCenter) < (_carousel.DistanceBetweenItems / 2)) // && indexChangable)
        {
          if (carousel.Direction > 0)
          {
            // Debug.Log("moving forward");
            if (item.TrueX < _initializer.carousel.ContainerCenter) { return _initializer.carousel.ItemsList.IndexOf(item); }
            else
            {
              //return items.IndexOf(item) - 1 < 0 ? items.Count - 1 : items.IndexOf(item) - 1;
              return Utilities.Mod(_initializer.carousel.ItemsList.IndexOf(item) - 1, _initializer.carousel.ItemsList.Count);
            }
          }
          else
          {
            // Debug.Log("moving in reverse");
            if (item.TrueX > _initializer.carousel.ContainerCenter) { return _initializer.carousel.ItemsList.IndexOf(item); }
            else
            {
              return (_initializer.carousel.ItemsList.IndexOf(item) + 1) % _initializer.carousel.ItemsList.Count;
            }
          }
        }
      }

      return -1; // fail, maybe a try catch above instead?
    } // used for drag distance adjust

    // from animator
    public void BuildItemList(Carousel carousel)
    {

      foreach (Transform item in _itemsContainer.transform)
        AppendItem(item, item.GetSiblingIndex(), carousel);
      //return;

      // swap to MEC, MEC in Carousel? Use GameObject container.name
      // so hopefully other carousels are named different, weapons, characters, etc.
      //_animator.StopMoveCoroutines(); // it's in CarouselAnimator.cs
      carousel.CurrentIndex = _carousel.ItemsList.Count > 1 ? 1 : 0; // hmmm cause divide by zero?
      _animator.ItemIndexUpdatedEvent?.Invoke(carousel.CurrentIndex);
      GameObject firstItem = _carousel.ItemsList[carousel.CurrentIndex].RectTrans.gameObject;
      List<GameObject> oneItem = new List<GameObject>() { firstItem };

      _initializer.StartCoroutine(_animator.MoveToItem(oneItem, carousel, this));
    }

    // from Menu, combine with AppendItem
    public GameObject AddItem(string imageName, bool checkForDuplicates, Carousel carousel)
    {
      // this method is run from event finished downloading event
      // private void AppendAvatarToMenu(RiAvatarUri uri)

      // if duplicate exists then replace at that index, otherwise add to end
      int index = _itemsContainer.transform.childCount;

      // bool new, string moved up
      // gameobject moved up and nulled, instead of declared below

      // delete duplicate incase model, image have changed their outfits/hair etc.
      if (checkForDuplicates)
      {
        // swap part of this into CarouselItems.cs
        foreach (Transform item in _itemsContainer.transform)
        {
          if (imageName == item.name)
          {
            index = item.transform.GetSiblingIndex();
            RemoveItem(item.gameObject, carousel);
            GameObject.Destroy(item.gameObject); // 
            break; // Only one match should be possible
          }
        }
      }

      // swap part of this into CarouselItems.cs,
      // 2025 _avatarImagePrefab should be private
      GameObject avatarImage = GameObject.Instantiate(_buttonEvents._avatarImagePrefab, _itemsContainer.transform);

      avatarImage.name = imageName; // uri.AbsoluteName;
      avatarImage.transform.SetSiblingIndex(index);

      // AvatarData lots of meta data, glb, paths, and a couple static variables
      // override method or move to another script
      // 2025 GenerateSprite in ButtonEvents should be triggered by ready player me
      //string avatarPngPath = $@"{AvatarData.AvatarPath}\{imageName}.png";
      //GenerateSprite(avatarImage.GetComponent<Image>(), avatarPngPath);

      // auto select newest avatar?
      //SetCurAvatarIndex(avatarImage.transform.GetSiblingIndex());

      bool canAppend = checkForDuplicates;
      if (canAppend) { AppendItem(avatarImage.transform, index, carousel); }

      return avatarImage;
    }



    // from Animator, combine with AddItem
    public void AppendItem(Transform itemTrans, int index, Carousel carousel)
    {
      // this isn't quite right
      _carousel.ItemsList.Insert(index, new Item(itemTrans));






      // cap this to max visible characters, and cap max characters to like 10?
      //_distanceBetweenItems = _containerWidth / items.Count;
      _carousel.DistanceBetweenItems = _carousel.ContainerWidth / _itemsContainer.transform.childCount;

      // swap to MEC, MEC in Carousel? Use GameObject container.name
      // so hopefully other carousels are named different, weapons, characters, etc.
      //_animator.StopMoveCoroutines();
      _animator.UpdatePositions(carousel.CurrentIndex, carousel);
      //return;

      carousel.CurrentIndex = itemTrans.GetSiblingIndex();
      _animator.ItemIndexUpdatedEvent?.Invoke(carousel.CurrentIndex);
      List<GameObject> oneItem = new List<GameObject>() { itemTrans.gameObject };

      _initializer.StartCoroutine(_animator.MoveToItem(oneItem, carousel, this));
      // if index is items.Count the emulate right(next?) click
      if (_carousel.ItemsList.Count - 1 == index)
      {
        //OnClickRight();
      }
      else
      {
        // this won't scale if it's in the proper position
      }

    }





    // from Animator
    public void RemoveItem(GameObject item, Carousel carousel)
    {
      _carousel.ItemsList.Remove(_carousel.ItemsList[carousel.CurrentIndex]);
      _carousel.DistanceBetweenItems = _carousel.ContainerWidth / _carousel.ItemsList.Count;

      _animator.UpdatePositions(0f, carousel); // adjusts spacing

      carousel.CurrentIndex--;
      _animator.ItemIndexUpdatedEvent?.Invoke(carousel.CurrentIndex);

      // set direction?
      _animator.DriftAmount += (DistanceToIndex(carousel.CurrentIndex, carousel) / _carousel.DistanceBetweenItems) * -1;

      GameObject prevItem = _carousel.ItemsList[carousel.CurrentIndex].RectTrans.gameObject;
      List<GameObject> oneItem = new List<GameObject>() { prevItem };

      _initializer.StartCoroutine(_animator.MoveToItem(oneItem, carousel, this));
    }

    // subscribed to ItemIndexUpdatedEvent in CarouselMenu.cs
    // that's run from CarouselAnimator.cs... too complicated
    // private void SetCurAvatarIndex(int nextAvatarIndex) {}
    // inacessible due to protection level. Public temporarily.
    public void SetCurAvatarIndex(int nextAvatarIndex, Carousel carousel)
    {
      if (carousel.CurrentIndex == nextAvatarIndex) { return; }

      // swap these into CarouselItems.cs
      GameObject curItem = _itemsContainer.transform.GetChild(carousel.CurrentIndex).gameObject;
      GameObject nextItem = _itemsContainer.transform.GetChild(nextAvatarIndex).gameObject;

      if (nextItem.name == AddAvatar)
      {
        _buttonStates.SwapLayout(ButtonStates.ButtonLayouts.AddItemLayout);
      }
      else if (curItem.name == AddAvatar)
      {
        _buttonStates.SwapLayout(ButtonStates.ButtonLayouts.ModifyItemLayout);
      }

      carousel.CurrentIndex = nextAvatarIndex;
    }



  }
}