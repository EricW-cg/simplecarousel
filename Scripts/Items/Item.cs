using UnityEngine;
using UnityEngine.UI;

namespace SimpleCarousel
{
  public class Item
  {
    public float TrueX { get; set; }                      // position before curve lerp adjustment
    public RectTransform RectTrans { get; private set; }  // set position after curve adjusted
    public Image AnImage { get; private set; }            // set image opacity/alpha
    public Canvas ACanvas { get; private set; }           // for clicking without button?

    public Item(Transform itemTrans) 
    {
      TrueX = itemTrans.position.x;

      RectTrans = itemTrans.GetComponent<RectTransform>();

      AnImage = itemTrans.GetComponent<Image>();
      AnImage.alphaHitTestMinimumThreshold = .5f;
      AnImage.maskable = false;
      AnImage.useSpriteMesh = true;

      ACanvas = itemTrans.GetComponent<Canvas>();
      ACanvas.overrideSorting = true;
    }
  }
}