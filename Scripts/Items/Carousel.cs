using System.Collections.Generic;
using System.ComponentModel;
using UnityEngine;

using MEC;

namespace SimpleCarousel
{
  public class Carousel
  {
    public string Name { get; private set; }

    private void Temp()
    {
      Timing.RunCoroutine(AMethod(), Name);
    }
    private IEnumerator<float> AMethod() { yield return Timing.WaitForOneFrame; }

    public List<Item> ItemsList { get; private set; } = new List<Item>();

    // This should be driven by two empty locators
    // for any direction carousels, maybe even curved
    // check obsidian
    public GameObject Container { get; private set; }
    public float ContainerWidth { get; private set; }
    public float ContainerMin { get; private set; }
    public float ContainerMax { get; private set; }
    public float ContainerCenter { get; private set; }
    public float DistanceBetweenItems { get; set; } // temp public set


    public AnimationCurve CurvePosition { get; private set; }
    public AnimationCurve CurveScale { get; private set; }
    public AnimationCurve CurveAlpha { get; private set; }


    public int CurrentIndex { get; set; } = 0;
    public int Direction { get; set; }

    // swap to more effective coroutines
    public Coroutine Drift { get; private set; }

    public Carousel(
      GameObject ItemsContainer,
      AnimationCurve curvePosition,
      AnimationCurve curveScale,
      AnimationCurve curveAlpha
    )
    {
      Name = ItemsContainer.name + ItemsContainer.GetInstanceID();
      Container = ItemsContainer;

      // what if the curves change during game?
      CurvePosition = curvePosition;
      CurveScale = curveScale;
      CurveAlpha = curveAlpha;

      // what if rect transform changes shape during game?
      RectTransform rt = Container.GetComponent<RectTransform>();
      ContainerWidth = rt.rect.width;
      Vector3[] v = new Vector3[4];
      rt.GetWorldCorners(v);
      ContainerMin = v[0].x;
      ContainerMax = v[3].x;
      ContainerCenter = ((ContainerWidth - ContainerWidth) / 2) + ContainerWidth;
    }



  }
}