using UnityEngine; // debug.log
using UnityEngine.UI;

namespace SimpleCarousel
{
  public class ButtonStates : IAwakable<Initializer>
  {
    // private Manager _carousel; 
    public enum ButtonLayouts { AddItemLayout, ModifyItemLayout }

    private Selectable[] _selectables; // Buttons and TMP_InputField in one
    private GameObject _downloadInput, _download, _add, _remove, _update, _select;

    public ButtonStates() { } // needed?


    public void DoAwake(Initializer initializer)
    {
      _selectables = new Selectable[]
      {
        initializer.ButtonEvents.DownloadInput,
        initializer.ButtonEvents.Download,
        initializer.ButtonEvents.Add,
        initializer.ButtonEvents.Remove,
        initializer.ButtonEvents.Update,
        initializer.ButtonEvents.Select,
      };

      _downloadInput = initializer.ButtonEvents.DownloadInput.gameObject;
      _download = initializer.ButtonEvents.Download.gameObject;
      _add = initializer.ButtonEvents.Add.gameObject;
      _remove = initializer.ButtonEvents.Remove.gameObject;
      _update = initializer.ButtonEvents.Update.gameObject;
      _select = initializer.ButtonEvents.Select.gameObject;
    }


    // add listener to DriftFinishEvent from CarouselAnimator?
    public void ToggleInteractions(bool toggle)
    {
      foreach (Selectable selectable in _selectables)
      {
        selectable.interactable = toggle;
      }
    }

    // inaccessible due to protection level, temp public
    public void SwapLayout(ButtonLayouts buttonLayouts)
    {
      bool isAddItemState = buttonLayouts == ButtonLayouts.AddItemLayout;
      _downloadInput.SetActive(isAddItemState);
      _download.SetActive(isAddItemState);

      bool isModifyItemState = buttonLayouts == ButtonLayouts.ModifyItemLayout;
      _add.SetActive(isModifyItemState);
      _remove.SetActive(isModifyItemState);
      _update.SetActive(isModifyItemState);
      _select.SetActive(isModifyItemState);
    }


  }
}