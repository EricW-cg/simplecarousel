using System.Collections.Generic;
using UnityEngine;

using UnityEngine.UI;                   // buttons and other ui stuff
using TMPro;                            // input field for ReadyPlayerMe url

using System.Threading.Tasks;
using System.IO;
using UnityEngine.Events;               // UnityActions?

using System.Collections;
using System;
using System.Linq;


// I bypass the need for demo.readyplayer.me completely because I don't use a character creator
// so I guess I don't need to become a partner for now as long as people create their character
// then paste the url? 
namespace SimpleCarousel
{
  // use an action instead?
  public delegate void MenuModifiedDelegate(string imageName);

  public class ButtonEvents : MonoBehaviour
  {
    public static MenuModifiedDelegate MenuModifiedEvent;

    //
    // [SerializeField] private ScrollRect _scrollRect;
    // [SerializeField] private RectTransform _contentPanel;

    // ready player me documentation
    // https://docs.readyplayer.me/integration-guides/unity

    // shouldn't require a player controller, should fire an
    // event so that another system can take care of that
    // require animal controller
    // require invector third person basic locomotion?
    // require invector first person?

    // settings required
    // Two cameras, first and third person
    // current player?

    private Initializer _initializer;

    // private Button _retryDownloadMultiplayerAvatars; // that's really long but gets the point across

    private Button[] _buttons;

    // buttons need to be redone for proper style guide stuff
    [field: Header("Menu Buttons")]
    [field: SerializeField] public TMP_InputField DownloadInput { get; private set; }
    [field: SerializeField] public Button Download { get; private set; }

    // despawn is backspace, pop instead of ragdoll? ha
    [field: Space(10)]
    [field: SerializeField] public Button Next { get; private set; }
    [field: SerializeField] public Button Previous { get; private set; }

    [field: Space(10)]
    [field: SerializeField] public Button Add { get; private set; }
    [field: SerializeField] public Button Remove { get; private set; }
    [field: SerializeField] public Button Update { get; private set; }
    [field: SerializeField] public Button Select { get; private set; }
    // close menu?
    //[field: SerializeField] public Button Close { get; private set; }

    [field: Space(10)]
    [field: SerializeField] public TextMeshProUGUI MessageArea { get; private set; }


    [Space(10)]
    // inaccessable to to protection level, temp public
    [SerializeField] public GameObject _avatarImagePrefab;
    //private GameObject _curAvatarImage; // default squidgames green female?
    private int _curAvatarIndex = 0; // default squidgames green female?
    //private PlayerManager _playerManager;    // error move to PlayerManager.cs?
    


    //private TextMeshProUGUI _messageArea;



    // private delegate void MenuStateDelegate();
    // private MenuStateDelegate MenuStateChangeEvent;

    // Test avatars
    // https://d1a370nemizbjq.cloudfront.net/831120c6-305f-4c03-97e8-14a9c7f9a7f9.glb Squid Games red
    // https://d1a370nemizbjq.cloudfront.net/2f6508b5-7dac-4b19-bda0-8acd936e2ac0.glb Mandy cool glasses
    // https://d1a370nemizbjq.cloudfront.net/87692a7d-12fb-4dde-8fe9-cae85652f082.glb Button up shirt
    // https://d1a370nemizbjq.cloudfront.net/f4939722-1f3c-4759-8127-f10e87136fef.glb Casey Jones
    // https://d1a370nemizbjq.cloudfront.net/209a1bc2-efed-46c5-9dfd-edc8a1d9cbe4.glb ready player me demo

    #region start awake setup button listeners


    private void Awake()
    {
      // getting CorouselAnimator
      //_carousel = transform.GetComponentInChildren<SimpleCarousel.CarouselAnimator>();

      //Debug.Log(Add.name);


      // maybe save _settings.ItemsContainer?
      //_items = transform.Find("Avatars").gameObject;
      //_messageArea = transform.Find("MessageArea").GetComponent<TextMeshProUGUI>();

      // error
      //_playerManager = GameObject.Find("PlayerManager").GetComponent<PlayerManager>();

      // where do we get Manager? Send in via some function? Find in scene?
      // prefer to pass in cause Find is rare in project. Could get set? or
      // custom function. What about static?
      //_carousel = carousel;

      



      // null reference, temp hide
      DownloadInput.onSelect.AddListener
      (
          (aString) =>
          {
            DownloadInput.text = GUIUtility.systemCopyBuffer;
            Debug.Log(GUIUtility.systemCopyBuffer); // la cocaina
          }
      );
    }

    private void Start()
    {
      // Not Unity's, Mine that I wrote!
      AvatarBuilder avatarBuilder = new AvatarBuilder();

      // menu avatar scroll
      // rewrite these for carousel animator



      // move these back to where they are used, keep ButtonEvents.cs simple and unaware
      // of who listens for the events
      // avatarBuilder needs to be built different
      // error rework for my AvatarBuilder
      //DownloadInput.onSubmit.AddListener((inputAvatarUrlText) => ClickEvent(async delegate 
      //{ 
      //  await avatarBuilder.DownloadAvatarAsync(inputAvatarUrlText); 
      //}));
      Download.onClick.AddListener(() => ClickEvent(async delegate
      {
        //await avatarBuilder.DownloadAvatarAsync(_downloadAvatarFld.text);
      }));

      // overwatch continue
      // select

      // update, select, remove avatars
      // error rework for my AvatarBuilder
      //Update.onClick.AddListener(() => ClickEvent(async delegate
      //{
      //  await avatarBuilder.UpdateAvatarAsync(GetCurAvatarGo());
      //}));
      //Remove.onClick.AddListener(() => ClickEvent(async delegate
      //{
      //  await avatarBuilder.DeleteAvatar(GetCurAvatarGo());
      //}));
      //Select.onClick.AddListener(() => ClickEvent(async delegate
      //{
      //  await avatarBuilder.SelectAvatar(_playerManager, GetCurAvatarGo());
      //}));
      // unspawn
      // close menu


      //_carousel.ItemIndexUpdatedEvent += SetCurAvatarIndex;
      // null reference, temp hide
      //_carousel.Animator.ItemIndexUpdatedEvent += _carousel.Items.SetCurAvatarIndex;

      // error rework my AvatarBuilder
      //AvatarBuilder.AvatarDownloadedEvent += AvatarDownloaded;
      //AvatarBuilder.AvatarRemovedEvent += _carousel.RemoveItem; // carousel
      ////AvatarBuilder.AvatarSelectedEvent += AvatarSelected;
      //AvatarBuilder.MessageEvent += LogMessageToMenu;
      //AvatarBuilder.DownloadFailEvent += ToggleButtonInteractions;


      // async await, anything under can run before
      // this is finished, carousel build items list
      // is at the end of this method.
      // _initializer.carousel is currently null reference
      //Debug.Log($"initializer.carousel: {_initializer}", _initializer);
      _initializer = GetComponent<Initializer>();
      BuildItemButtons(avatarBuilder, _initializer.carousel);

    }

    #endregion


    private delegate Task ButtonActions();
    private async void ClickEvent(ButtonActions ButtonAction)
    {
      bool on = true, off = false;

      _initializer.ButtonStates.ToggleInteractions(off);

      await ButtonAction();

      _initializer.ButtonStates.ToggleInteractions(on);
    }



    // subscribed to message event in RcAvatarManager.cs
    private void LogMessageToMenu(string message) => MessageArea.text = message;


    #region Avatar get set methods

    private GameObject GetCurAvatarGo()
    {
      // added as listener to buttons in Start()

      // I'm thinking if I change this script to run off of index
      // instead of gameobject for selected avatar then return this
      //_avatarImagesContainer.transform.GetChild(_selectedAvatarIndex).gameObject

      // swap this into CarouselItems.cs
      return _initializer.ItemsContainer.transform.GetChild(_curAvatarIndex).gameObject;
    }

    // move to CarouselItems.cs, subscribed to ItemIndexUpdatedEvent
    // private void SetCurAvatarIndex(int nextAvatarIndex) {}
    

    private void AvatarSelected(GameObject avatarImage)
    {
      // maybe rename method to AvatarSpawned

      // send out avatarImage name as the new spawned avatar
      // over multiplayer network for all other clients to download
      // _playerManager = avatarController;
    }

    #endregion Avatar get set methods

    #region previous next methods

    private void OnClickPrevious(Carousel carousel)
    {
      _initializer.Animator.DriftAmount--;
      _initializer.carousel.CurrentIndex = (_initializer.carousel.CurrentIndex + 1) % _initializer.carousel.ItemsList.Count;
      _initializer.Animator.ItemIndexUpdatedEvent?.Invoke(_initializer.carousel.CurrentIndex);

      if (_initializer.Animator._isDrifting) { return; }
      _initializer.Animator.SetDirection(-1, carousel);

      // swap to MEC, MEC in Carousel? Use GameObject container.name
      // so hopefully other carousels are named different, weapons, characters, etc.
      //_initializer.Animator._drift = _initializer.StartCoroutine(_initializer.Animator.Drift(_initializer, _initializer.carousel));
    }

    private void OnClickNext(Carousel carousel)
    {
      _initializer.Animator.DriftAmount++;
      _initializer.carousel.CurrentIndex = Utilities.Mod(_initializer.carousel.CurrentIndex - 1, _initializer.carousel.ItemsList.Count);
      _initializer.Animator.ItemIndexUpdatedEvent?.Invoke(_initializer.carousel.CurrentIndex);

      if (_initializer.Animator._isDrifting) { return; }
      _initializer.Animator.SetDirection(1, carousel);

      // swap to MEC, MEC in Carousel? Use GameObject container.name
      // so hopefully other carousels are named different, weapons, characters, etc.
      //_initializer.Animator._drift = _initializer.StartCoroutine(_initializer.Animator.Drift(_initializer, _initializer.carousel));
    }

    #endregion previous next methods

    #region Menu building/adding/removing

    // coroutines can be run from async methods
    // but they only wait inside themselves..if that makes sense
    // private IEnumerator EnumerateAvatarFiles()
    // {
    //     yield return new WaitForEndOfFrame();
    // }

    // move to CarouselItems.cs? Or not, buttons are menu related?
    private async Task BuildItemButtons(AvatarBuilder avatarBuilder, Carousel carousel)
    {
      // should this method be async await?
      // Or maybe Enumerate or Coroutine
      // could limit max avatars to 10 or 20 then
      // force some removals to add more

      // class with static async task one method to move files to documents folder.
      // override method or move to another script
      //await PreloadDefaultAvatars.MoveStreamingToMyDocs();
      // StartCoroutine(EnumerateAvatarFiles()); // if directory gets big or slow

      SortedDictionary<System.DateTime, string> avatarPngs = new SortedDictionary<System.DateTime, string>();

      // tenSeconds is january 1, 0001 00:00:10 I think
      // 2025 strings for .SquidFemale and .SquidMale don't exist in the Unity project
      // they do exist in documents folder ReadyController
      long tenSeconds = 100_000_000, twentySeconds = 200_000_000, thirtySeconds = 300_000_000;
      avatarPngs.Add(new System.DateTime(tenSeconds), ItemsModifier.AddAvatar); // name probably doesn't matter
      avatarPngs.Add(new System.DateTime(twentySeconds), ItemsModifier.SquidFemale); // default female
      avatarPngs.Add(new System.DateTime(thirtySeconds), ItemsModifier.SquidMale); // default male


      // 2025 because carousel is independent of ready player me some of this should
      // be run from ready player me scripts? glb and json are irrelevant to carousel 
      // this could trigger a check or validate event to see if image can be added

      // EnumerateFiles() might be more efficient
      // Directory.GetFiles sort order is not guaranteed, use Array.Sort if order is important
      // AvatarData lots of meta data, glb, paths, and a couple static variables
      // override method or move to another script
      //string[] fileEntries = Directory.GetFiles(AvatarData.AvatarPath);
      //foreach (string fileEntry in fileEntries)
      //{
      //  // these two are auto added above with avatarPngs.Add
      //  if (fileEntry.Contains(SquidFemale) || fileEntry.Contains(SquidMale)) { continue; }

      //  bool isPngFile = fileEntry.Contains(".png", System.StringComparison.CurrentCultureIgnoreCase);
      //  if (isPngFile)
      //  {
      //    // Debug.Log(Path.GetFileNameWithoutExtension(fileEntry));
      //    // does .glb exist, does .json exist? If they do add avatar png, 
      //    // if not add error.png?
      //    // or possibly just remove the files that don't have all three
      //    // or use AvatarManager to repair/replace avatars missing files

      //    avatarPngs.Add(
      //      File.GetCreationTime(fileEntry), Path.GetFileNameWithoutExtension(fileEntry)
      //    );
      //  }

      //}

      // 2025
      //return;

      // 2025 adds default avatar images, forgets the images...
      // i starts at one to bypass the add avatar spot
      // swapping to true in AddItem prevents Add Item image from moving
      // into a weird spot and having his alpha turned to 0, default was false
      GameObject avatarImage;
      for (int i = 1; i < avatarPngs.Count; i++) 
      {
        avatarImage = _initializer.ItemsModifier.AddItem(avatarPngs.ElementAt(i).Value, true, carousel);
        avatarImage.transform.SetSiblingIndex(i);
      }

      

      // maybe run event here
      // Animator isn't instantiated yet...
      // positions avatar images
      _initializer.ItemsModifier.BuildItemList(carousel);
    }

    // this should be specific to ReadyController
    private void AvatarDownloaded(string imageName, Carousel carousel)
    {
      _initializer.ItemsModifier.AddItem(imageName, true, carousel); // and select?

      // if player has spawned avatar that is the one just downloaded, then update it
      // playermanager has what controller to use, and what avatardata for that player
      // overridemethod or move to another script
      //if (_playerManager.AvatarData.AbsoluteName == imageName)
      //{
      //  // current player controller is the newly refreshed or downloaded character
      //  // not Unity's AvatarBuilder...My AvatarBuilder tm copyright..
      //  var avatarBuilder = new AvatarBuilder();
      //  ClickEvent
      //  (
      //      async delegate { await avatarBuilder.SelectAvatar(_playerManager, GetCurAvatarGo(), true); }
      //  );
      //}

      // return AppendAvatarToMenu(imageName, true);

    }

    

    // move to CarouselItems.cs or specific to ReadyController
    private void GenerateSprite(Image avatarImage, string avatarPngPath)
    {
      // maybe move this method into a utility script?
      // Needs more settings I think.

      byte[] textureBytes = File.ReadAllBytes(avatarPngPath);
      Texture2D loadedTexture = new Texture2D(0, 0);
      loadedTexture.LoadImage(textureBytes);

      // avatarImage = avatarImagesContainer.transform.GetChild(0).GetComponent<Image>();
      avatarImage.sprite =
        Sprite.Create(
          loadedTexture,
          new Rect(0f, 0f, loadedTexture.width, loadedTexture.height),
          Vector2.zero
        );

      // modified in CarouselAnimator
      //avatarImage.maskable = false;
      //avatarImage.useSpriteMesh = true;
    }


    #endregion Menu building/adding/removing


  }

}