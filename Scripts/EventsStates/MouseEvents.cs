using System;
using UnityEngine;
using UnityEngine.EventSystems;

using MEC;

namespace SimpleCarousel
{
  public class MouseEvents : MonoBehaviour,
      IPointerDownHandler, IPointerUpHandler,
      IBeginDragHandler, IDragHandler, IEndDragHandler,
      IScrollHandler
  {
    
    // removed actions, reminder of what they look like for me
    // if you are not me delete these comment lines :P
    // public event Action<PointerEventData> DoPointerDown;

    private void Awake() { }
    private void Start() { }

    public void OnPointerDown(PointerEventData e) => PassPointerDownData();  // mouse click, finger touch?
    public void OnPointerUp(PointerEventData e) => PassPointerDownData();   // mouse click, finger release?

    public void OnBeginDrag(PointerEventData e) => PassPointerDownData();   // mouse drag, finger drag?
    public void OnDrag(PointerEventData e) => PassPointerDownData();        // mouse drag, finger drag?
    public void OnEndDrag(PointerEventData e) => PassPointerDownData();     // mouse drag, finger drag?

    public void OnScroll(PointerEventData e) => PassPointerDownData();


    private void PassPointerDownData()
    {
      // Animator OnPointerDown needs no PointerEventData
    }
    public void AnAnimatorMethod() { }


  }
}