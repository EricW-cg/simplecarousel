using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.EventSystems;

namespace SimpleCarousel
{
  public class CarouselPointerEventData : PointerEventData
  {
    public Initializer Initializer { get; set; }
    public MouseEvents JustForFunTest { get; set; }

    //PointerEventData line 266
    public CarouselPointerEventData(EventSystem eventSystem) 
      : base(eventSystem) { }

  }
}