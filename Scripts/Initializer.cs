using TMPro;
using UnityEditor; // remove after moving CarouselSettingsEditor
using UnityEngine;
using UnityEngine.UI;

using SimpleCarouselEditor; // will this cause build error?
using System.Collections.Generic;
using System.Collections;
using UnityEngine.EventSystems;

namespace SimpleCarousel
{

  public class Initializer : MonoBehaviour
  {
    // properties can be virtual, didn't know
    [field: Header("Items Parent Container")]
    [field: SerializeField] public GameObject ItemsContainer { get; private set; }

    // 2025 ButtonEvents AvatarImagePrefab currently used instead of this, swap?
    [field: SerializeField] public GameObject ItemPrefab { get; private set; } 

    [field: Header("Animation Variables")]
    [field: SerializeField]
    [field: Range(0, 100)]
    public float DragSpeed { get; private set; } = 20f;

    [field: SerializeField]
    [field: Range(0, 100)]
    public float DriftFriction { get; private set; } = 50f; // not used atm

    [Header("Animation Curves")]
    [SerializeField] private AnimationCurve _curvePosition;
    [SerializeField] private AnimationCurve _curveScale;
    [SerializeField] private AnimationCurve _curveAlpha;

    // other scripts, try to reduce the need for these
    public MouseEvents MouseEvents { get; private set; }    // monobehaviour to get events
    public ButtonEvents ButtonEvents { get; private set; }  // monobehaviour for button events
    public ButtonStates ButtonStates { get; private set; }  // not a monobehaviour
    public Carousel carousel { get; private set; }          // not a monobehaviour
    public ItemsModifier ItemsModifier { get; private set; }// not a monobehaviour
    public Animator Animator { get; private set; }          // not a monobehaviour

    private void Reset()
    {
      // curves go to Items? now that Items has been renamed carousel
      // it make a lot of sense. Move them, or send/cache/inject, whatever
      _curvePosition = CarouselSettingsEditor.ResetCurvePosition(); // curve depth
      _curveScale = CarouselSettingsEditor.ResetCurveScale();
      _curveAlpha = CarouselSettingsEditor.ResetCurveAlpha();

      // curve "up" and curve "down"???

    }

    // Awake can be protected virtual void Awake for overriding
    // add/get components, new classes but don't pass into constructors
    // pass into DoStart instead, order of initialization will be safe
    private void Awake()
    {
      // these need instances for each carousel menu
      MouseEvents = ItemsContainer.AddComponent<MouseEvents>();
      ButtonEvents = GetComponent<ButtonEvents>();
      ButtonStates = new ButtonStates();
      carousel = new Carousel(ItemsContainer, _curvePosition, _curveScale, _curveAlpha);

      // these should be singletons or static
      // if ItemsModifier.Instance == null?? for singletons
      // if static then these aren't needed here
      ItemsModifier = new ItemsModifier();
      Animator = new Animator();

      ButtonStates.DoAwake(this);
      ItemsModifier.DoAwake(this);
      //Animator.DoAwake(this);
    }

    private void Start() 
    {
      // it's not time related. Something has to be done first
      // yield return new WaitForSeconds(2f);

      //ItemsModifier.BuildItemList();
      //Animator.UpdatePositions(0);
    }
  }
}