<!-- <p align="center">
  <a href="" rel="noopener">
 <img width=200px height=200px src="https://i.imgur.com/6wj0hh6.jpg" alt="Project logo"></a>
</p> -->

<h1 align="center">Simple Carousel</h1>

<!-- align="center" -->
<div>

[![Status](https://img.shields.io/badge/status-active-success.svg)]()
[![GitHub Issues](https://img.shields.io/badge/issues-0-yellow)](https://img.shields.io/badge/issues-0-yellow)
[![License](https://img.shields.io/badge/license-MIT-blue.svg)](/LICENSE)

</div>

---

<p align="center"> A simple carousel to scroll items in Unity Engine.
    <br> 
</p>

## 📝 Table of Contents

- [Getting Started](#getting_started)
- [Usage](#usage)
- [TODO](#todo)
- [Built Using](#built_using)
- [Authors](#authors)
- [Acknowledgments](#acknowledgement)


## 🏁 Getting Started <a name = "getting_started"></a>

Download the Simple Carousel using one of these methods:
- Unity package manager download from git url and download samples
- Clone into Assets folder with git url
- Download the repository and drop into Assets folder

For second two options the samples folder is hidden in Unity Project tab. Open windows explorer and find the samples folder and extract the unity package file into the project.

## 🎈 Usage <a name="usage"></a>

Add notes about how to use the tool.

## 📋 Todo <a name="todo"></a>

Some stuff that could be implemented, todo, roadmap, backlog.

## ⛏️ Built Using <a name = "built_using"></a>

- [Unity Engine](https://unity.com/)

## ✍️ Authors <a name = "authors"></a>

- [@EricW-cg](https://gitlab.com/EricW-cg/simplecarousel)

See also the list of [contributors](https://github.com/kylelobo/The-Documentation-Compendium/contributors) who participated in this project.

## 🎉 Acknowledgements <a name = "acknowledgement"></a>

- Hat tip to anyone whose code was used
- Inspiration
- References
